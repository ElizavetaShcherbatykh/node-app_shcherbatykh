const process = require('process');
const vers = require('./package.json');
console.log('Hi, I\'m a simple app. I can do only 2 things')

if (process.argv.includes('-v')) {
    require('./version')
} else {
    null
}

if (process.argv.includes('--version')) {
    require('./version')
} else {
    null
}

if (process.argv.includes('-h' || '--help')) {
    require('./help')
} else {
    null
}

if (process.argv.includes('--help')) {
    require('./help')
} else {
    null
}